import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import { Button } from 'react-bootstrap';

class Secret extends Component {
    constructor() {
      super();
      //Set default message
      this.state = {
        message: 'Loading...'
      }
    }
    
    componentDidMount() {
      //GET message from server using fetch api
      fetch('/api/secret')
        .then(res => res.text())
        .then(res => this.setState({message: res}));
    }
    render() {
      return (
        <div>
          <h1 style={{color: 'black'}}>Secret</h1>
          <p style={{color: 'black'}}>{this.state.message}</p>
        </div>
      );
    }
  }
  export default Secret;