import React, { Component } from 'react';
import '../css/home.css';
import { Button, Navbar, Nav, Form, FormControl } from 'react-bootstrap';


class Footer extends React.Component {
    render() {
        return (
            <Navbar bg="dark" variant="light" sticky='bottom'>
                <Navbar.Brand href="#home">JM</Navbar.Brand>
                <Nav className="navbar">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href='/construction'>Projects</Nav.Link>
                    <Nav.Link href='/Weather'>Weather</Nav.Link>
                    <Nav.Link href="/construction">Photography</Nav.Link>
                </Nav>
                <Form inline>
                    <Nav.Link href='/construction'>Contact</Nav.Link>
                </Form>
            </Navbar>


        )
    }
}

export default Footer;