import React, { Component } from 'react';
import './App.css';
import {Switch, Route} from 'react-router-dom';
import Home from './pages/Home';
import Weather from './pages/weather';
import CurrentWeather from './pages/CurrentWeather';
import UnderConstruction from './pages/construction';
import Secret from './components/secret';
import Users from './pages/users';
class App extends Component {
  render() {
    const App = () => {
      return (
      <div className="App">
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route exact path='/Weather' component={Weather}/>
          <Route exact path='/current-weather' component={CurrentWeather}/>
          <Route exact path='/construction' component={UnderConstruction}/>
          <Route exact path='/api/secret' component={Secret}/>
          <Route exact path='/users' component={Users}/>
        </Switch> 
      </div>
      )}
    return (
      <Switch>
        <App/>
      </Switch>
    );
  }
}

export default App;
