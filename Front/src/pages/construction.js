import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import { Button } from 'react-bootstrap';
import '../css/construction.css';

class UnderConstruction extends Component {
    render() {
        return (
            <div className='App' >
                <h1 className="title">Oops...</h1>
                    <p className='info'>This page is currently under construction. <br></br>Come back later!</p> 

                <Link to={'./'}>
                    <Button varient='raised'>
                        Go Home
                    </Button>
                </Link>
                
            </div>
        );
    }
}
export default UnderConstruction;