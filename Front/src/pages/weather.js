import React from 'react';
import { Dropdown, Button  } from 'react-bootstrap';
import { Form, FormGroup, Label, Input, Col, Row} from 'reactstrap';
import { Link } from 'react-router-dom';
import '../css/weather.css';
import style from 'bootstrap/dist/css/bootstrap.css';
import weatherModal from '../components/weatherModal.js'


const Weather = () => {
    return (
        <div className='background' >
            <div className='info-box'>
           
                <h2 >Weather Forcast</h2>
            
                <p >Enter a location below to get the current weather conditions for that place.</p>
            <weatherModal show='true'/>

        <div className='form-input' >
            <div className='country-input' >
            <Row>
            <Col sm="12" md={{ size: 6, offset: 3 }} >
            <Form action='/search-location' method='POST' >
                
                    <Label for="exampleEmail">Location</Label>
                    <Input type="text" name="location"  placeholder="Location" md='6' />
                    <Button variant='success' type='submit'>Get Weather</Button>
                
                
            </Form>

            </Col>
            </Row>
            {/*<div className='back-button'>
            <Link to={'./'}>
                <button type="button" class="btn btn-light">Back</button>
            </Link>
    </div>*/}
            </div>
           </div>
            

        </div>
        </div>


    )
}
export default Weather;