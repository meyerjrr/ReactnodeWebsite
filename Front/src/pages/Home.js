import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import TextLoop from "react-text-loop";
import '../css/home.css';
import { Button } from 'react-bootstrap';
import  NavBar  from '../components/navbar';
import Footer from '../components/footer';
import  GoDown  from '../components/goDown';
import { Col, Row, Container } from 'react-bootstrap';
import LazyHero from 'react-lazy-hero';
import img from '../assets/backgroundimg.jpg';


//style={ { backgroundImage: `url(${backgroundImage})`, height: '100%' } }
class Home extends Component {
    render() {
        return (
            <div className='App' >
                <NavBar/>
                
                    <LazyHero imageSrc={img} opacity='0.2' minHeight='100%' maxWidth='100%' isCentered='false'>
                        <h2>Jack Meyer</h2>
                        <span> </span><TextLoop fade='false' springConfig={{ stiffness: 1000, damping: 35 }} mask='true' interval='2000'>
                            <span>Student</span>
                            <span>Software Engineer</span>
                            <span>Film Photographer</span>
                                
                        </TextLoop>
                    </LazyHero>
                 
            <div className='infoSect'>
                <h2>About Me</h2>
                <div className='about-me'>
                    <Col lg={12}><span>I'm a 20 year old programmer from Perth, Australia and basically I had no clue what kind of development I wanted to go into. 
                        I picked up web development working at a start up and had an absolute blast doing it. Making me go out on my own to learn and find out more about it. 
                        I decided to make this website to track what im doing, and as a place to put milestones that I acheive, I hope you enjoy. </span></Col>
                </div>
                <div className='info-table'>
                <Container>
                    <Row>
                        <Col>Tech Stack </Col>
                        <Col>Links</Col>
                        <Col>Projects</Col>
                    </Row>
                </Container>
                </div>
            
            </div>
            <div className='project-sect'>
                <div className='projects'>
                <Row>
                    <Col xs={{span: 4, offset: 2}}>Current Projects</Col>

                    <Col  xs={4}>Future Projects</Col>
                    
                </Row>
                </div>
            </div>
            <div>
                <Footer/>
            </div>

            </div>

            
           
        );
    }
}
export default Home;