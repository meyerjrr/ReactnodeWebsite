import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import '../css/CurrentWeather.css';

//Assets
import ThunderStormIcon from '../assets/weather_icons/01W.svg';
import RainIcon from '../assets/weather_icons/02W.svg';
import SnowIcon from '../assets/weather_icons/03W.svg';
import ClearIcon from '../assets/weather_icons/04W-DAY.svg';
import CloudsIcon from '../assets/weather_icons/05W.svg';
import NoLocationFound from '../assets/no-location.svg';
import LoadingIcon from '../assets/loading.svg';


class CurrentWeather extends Component {
	constructor(props) {
		super(props);

		   this.state = ({
		      isLoading: true,
		      currentTemp: '',
		      humidity: '',
		      wind: '',
		      windDirection: '',
		      currentCondition: '',
		      currentConditionDescription: '',
		      weatherIcon: '',
		      cityName: '',
				cityNotFound: '',
				MeyerMeter: '',
				ClothingRecommended:'',
				isPM: 'pm',
		   })
	}

	 

	componentDidMount() {
		fetch('/search-location-weather')
		.then(res => res.json())
		.then(data => {
			if(data.data.cod === '404') {
				this.setState({
					isLoading: false,
					cityNotFound: '404'
				})
			} else {
			   // Determine weather icon
			   let weatherId = data.data.weather[0].id;
			
			   if(weatherId <= 232) {
			        this.setState({ weatherIcon: ThunderStormIcon })
			   } else if(weatherId >= 300 && weatherId <= 531) {
			        this.setState({ weatherIcon: RainIcon });
			   } else if(weatherId >= 600 && weatherId <= 622 ) {
			        this.setState({ weatherIcon: SnowIcon });
			   } else if(weatherId === 800) {
			        this.setState({ weatherIcon: ClearIcon });
			   } else if(weatherId >= 801 && weatherId <= 804) {
			        this.setState({ weatherIcon: CloudsIcon });
				}

				
				
			     this.setState({
			        isLoading: false,
			        currentTemp: Math.round(data.data.main.temp-273.15) + '°',
			        humidity: data.data.main.humidity + '%',
			        wind: Math.round(data.data.wind.speed) ,
			        windDirection: data.data.wind.deg,
			        currentCondition: data.data.weather[0].main,
			        currentConditionDescription: data.data.weather[0].description,
			        cityName: data.data.name
				  });
				  const temp = this.state.currentTemp;
				  //check to see the meyermeter
				if(temp<'15°'){
					this.setState({MeyerMeter:'Pretty Cold Out'});
					this.setState({ClothingRecommended:'Definitly Pants and jacket weather.'})}
				else if(temp > '15°' && temp<'25°' ) {
					if(this.state.wind > '20') {
						this.setState({MeyerMeter: 'Good weather out, but windy :/'});
						this.setState({ClothingRecommended:'Pants and shirt, possibly a jacket'})}
						else {
						this.setState({MeyerMeter:'Perfect weather out today, make the most of it!'});
						this.setState({ClothingRecommended:'Pants and shirt, possibly a jacket'})}}
				else if(temp>'30°') {
					this.setState({MeyerMeter:'Too hot '});
					this.setState({ClothingRecommended:'Probably shorts and shirt'})
				}
				  console.log(this.currentTemp);
			}
		})
		.catch(err => {
		   console.log(err);
		})	

		

		
	}

	render() {
		var days = [
			"Sunday",
			"Monday",
			"Tuesday",
			"Wednesday",
			"Thursday",
			"Friday",
			"Saturday"
		];

		var today = new Date();
		var date = today.getDay();
		const currentDay = days[date];
		console.log(currentDay);

		if(today.getHours > 12) {
			this.setState({isPM: 'am'})
		}

		var time = today.getHours()-12 + ":00" + this.state.isPM;

		const WeatherCardError = (
      <div className='errorTime'>
        <div className="weatherCardContainerError">
          <div className="weatherCardError">
			 	<img src={NoLocationFound}></img>
            <p> Whoa! Looks like there was an error with your zipcode.</p>
            <Link to="/weather">
              <Button variant="success">Try Again</Button>
            </Link>
          </div>
        </div>
      </div>
    );
		{console.log(this.state.MeyerMeter)};
		const WeatherConditions = (
			this.state.cityNotFound == 404 ? <div> { WeatherCardError } </div> :
			<div className='weatherContainer'>
			<div className='app'>
			   <div className='homeBtn'>
				     <Link to='/weather'><Button>Go Back</Button></Link>
			   </div>
			   <div className='weatherCardContainer'>
			      <div className='weatherCard'>
				<img src={this.state.weatherIcon} alt='Weather icon'/>
				   <div className='conditionsOverview'>
						<h4> {this.state.cityName} </h4>
						<p>{currentDay + ' ' + time}</p>
				      <p>{this.state.currentTemp}</p>
				      <p>{this.state.currentConditionDescription}</p>
				   </div>
				   <div className='conditionDetails'>
				      <p>Humidity: {this.state.humidity} </p>
				      <p>Wind Speed: {this.state.wind} mph</p>
				   </div>
			      </div> 
			     
				  <p>Clothing Recommended: {this.state.ClothingRecommended}</p>
			   </div>
			</div>
			</div>
		)

		const LoadingDisplay = (
		   <div className='loading'>
		      Loading...
		   </div>
		)

		const CurrentWeatherCard = ( 
		   this.state.isLoading === true ? <div> {LoadingDisplay} </div> : <div> {WeatherConditions} </div>
		)

		return (
		   <div>
	             { CurrentWeatherCard }
		   </div>
		)
	}
}

export default CurrentWeather;
