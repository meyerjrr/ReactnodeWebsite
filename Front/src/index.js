import React from 'react';
import { render } from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom'
import { Route } from 'react-router';
import App from './App';
import * as serviceWorker from './serviceWorker';
import home from './pages/Home';
import Weather from './pages/weather';
import CurrentWeather from './pages/CurrentWeather';



render((
    <Router>
        <App>
            <Route exact path='/' component={home}/>
            <Route exact path='/current-weather' component={CurrentWeather}/>
            
            {/*<Route exact path='/error' component={errorDisplay}/>*/}
        </App>
    </Router>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
