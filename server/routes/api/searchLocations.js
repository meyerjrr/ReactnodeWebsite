const fetch = require('node-fetch');
//example api call
//https://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=d7d3644c758cb241b51feadbd0d31230

module.exports = (app) => {
    let location;
    let country;

    app.post('/search-location', (req, res) => {
        
        location = req.body.location;
        //country = req.body.country;
        console.log(location);
        
        res.redirect('/current-weather');
        
        
    })


    app.get('/search-location-weather', (req, res) => {
        //https://api.openweathermap.org/data/2.5/weather?q=perth,au&appid=d7d3644c758cb241b51feadbd0d31230
        const baseURL = 'http://api.openweathermap.org/data/2.5/weather?q=';

        const apiId = `&appid=d7d3644c758cb241b51feadbd0d31230`;
        //formatting the url that will be used for the api 
        const userLocation = (url, place, apiID) => {
            let newUrl = url + place + apiID;
            return newUrl;
        };
        console.log(baseURL);
        console.log(location);
        console.log(apiId)

        const apiUrl = userLocation(baseURL, location, apiId);

        fetch(apiUrl)
        .then(res=>res.json())
        .then(data => {
            console.log({data})
            res.send({data});
        })
        .catch(err => {
            res.redirect('/current-weather');
        });
    })
}