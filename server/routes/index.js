const fs = require('fs');
require("module-alias/register");


module.exports = (app) => {
    fs.readdirSync(`routes/api/`).forEach(file => {
        /*kept picking up "" as a file location and was trying to
        open ./api/ and would crash */ 
      if (`${file.substr(0, file.indexOf("."))}` == "") {
        console.log("only want files");
      } else {
        require(`./api/${file.substr(0, file.indexOf("."))}`)(app);
      }
    });
}