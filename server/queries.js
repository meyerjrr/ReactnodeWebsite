const { Pool } = require('pg');
const pool = new Pool({
    user: 'me',
    host: 'localhost',
    database: 'api',
    password: 'password',
    port: '5432',
});
//get all users from the database
const getUsers = (request, responce) => {
    pool.query('SELECT * FROM users ORDER BY id ASC', (error, results) => {
        if(error){
            throw error;
        } 
        console.log(results.rows);
        if(results.rowCount > 0) {
            responce.status(200).json(results.rows);
        } else {
            responce.status(418).send('No tea');
        }
    })
}

//get a user by a single user id
const getUserById = (request, responce) => {
    pool.query(`SELECT * FROM users WHERE id = $1`, [id], (error, results) => {
        if(error){
            throw error;
        }
        responce.status(200).json(results.rows);
    })
}

//post a new user to the data base
const createUser = (request, responce) => {
    const {name, email} = request.body

    pool.query('INSERT INTO users (name, email) VALUES ($1, $2)', [name, email], (error, result) => {
        if(error) {
            throw error;
        }
        console.log(result);

        responce.status(201).send(`User added with id: ${result.insertId}`)
    })
}

//update the values of a user using put
const updateUser = (request, responce) => {
    const id = parseInt(request.params.id)
    const {name, email} = request.body

    pool.query(
        'UPDATE users SET name = $1, email = $2 WHERE id = $3', [name, email, id],
        (error, results) => {
            if(error){
                throw error
            }
            responce.status(200).send(`user modified with ID: ${id}`)
        })
}

//delete a user from the database
const deleteUser = (request, responce) => {
    const id = parseInt(request.params.id)

    pool.query('DELETE FROM users WHERE id = $1', [id], (error, responce) => {
        if(error){
            throw error 
        }
        responce.status(200).send(`user deleted with ID: ${id}`)
    })

}

module.exports = {
    getUsers,
    getUserById,
    createUser, 
    updateUser,
    deleteUser,
}