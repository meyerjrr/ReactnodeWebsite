const express = require('express');
const app = express();
const port = 5000;
const User = require('./models/userModel');
const db = require('./queries');
app.use(express.urlencoded({extended: true}));
app.use(express.json())

require("./routes")(app);


app.get('/', (req, res) => {
    res.send('PORT 5000');
})

app.get('/gettest'), (req, res) => {
    res.send('endpoint');
}

app.listen(port, (err) => {
    if(err) {console.log(err) };
    console.log('listening on port' + port);
})



app.get("/users", db.getUsers);

app.get("/getUsers", (req, res) => {
    res.json(db.getUsers);
});

app.get("/users/:id", db.getUserById);

app.post("/users", db.createUser);

app.put("/users/:id", db.updateUser);

app.delete("/users/:id", db.deleteUser);